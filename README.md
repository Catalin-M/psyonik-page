# Psyonik.tech  

Welcome to the repository for my simple Hugo website.  

The code here was obtained by running `hugo new site` and adding files individually. I didn't much like the themes I saw online and I wanted to build it from scratch, so I made it myself. If you visit the page you'll see that it uses minimal styling in line with my abilities.  

I considered text to be the main focus of the page itself, so I kept any additions to a minimum. Feel free to copy it and reuse as you see fit.  

I will be adding posts from time to time and I might consider migrating it to a VPS but I will keep this repository online at that point, updating the contents of the `/content/posts` folder only. 
