---
title: pSYoniK - Software, Hardware and Tech - In.That.Order.
menu: 
  navigation:
        name: "Home"
---

### Who I am

I've been interested in technology most of my life, whether that came in the shape of a console, a desktop PC or a metal model construction set. 

My first PC was a Compaq desktop running an Intel Pentium MMX at 166Mhz, 32 MB of RAM and a massive 800MB HDD which stored a Hungarian version of Windows 95. It was that initial system that thought me how to reinstall Windows (not an easy task in '99), not to delete system files, write game server scripts, create maps in a variety of games and generally enjoy technology. 

The purpose of this page is to have a place where I can collate my thoughts, anything new I learned or found interesting. Despite my passion and love for technology, I believe that it should first and foremost act as a tool to make our lives better. As such, I prefer simple and clean technical solutions that don't invade your privacy, vie for your attention or expose and gather information unnecessarily. To see any new posts you can subscribe to the **[RSS Feed](https://psyonik.tech/index.xml)** of this page. 

If you wish to contact me regarding any of the content on this page or for any other reason you can do so on **`catalinm`** at -this domain-. 

> "Medicine, law, business, engineering, these are all noble pursuits, and
necessary to sustain life. But poetry, beauty, romance, love, these are what we stay alive for." - Dead Poets Society