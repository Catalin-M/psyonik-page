---
title: "Migrating my website and digital minimalism"
date: 2021-02-25T12:36:15Z
draft: false
slug: "Migrating my Website"
summary: "Over the past few months I went on a spree of sorts. This wasn't a spending spree or focused on anything consumption oriented, but rather the other way around. I decided that I had (still have, if I'm honest...) too large of a digital footprint. Too many apps, too many accounts and too much tech to the point where it was decreasing the quality of my experience"
---

### Reducing my footprint

Over the past few months I went on a spree of sorts. This wasn't a spending spree or focused on anything consumption oriented, but rather the other way around. I decided that I had (still have, if I'm honest...) too large of a digital footprint. Too many apps, too many accounts and too much tech to the point where it was decreasing the quality of my experience.  

![Image of smartphone screen](/images/apps.jpg)  
##### Image credited to [Rami Al-zayat](https://unsplash.com/photos/w33-zg-dNL4)  

The whole thing started while I worked on my final year school project (which I will upload over the next few days to my Gitlab). The application would connect to the Facebook Graph API and The Guardian API. For a post (or posts) for a given day, a request would be sent to the Guardian API to retrieve news articles from that day. The two bits of data, the post and the news article, would be merged and put side by side. The goal was to try and juxtapose the duality of technology and our lives. While Facebook aims to keep you entertained at all costs, mindlessly scrolling through the feed, The Guardian is here to provide you with a clearer look at the world and the things that are very wrong in it.  

The premise, which is a simplistic approach to the problem, got me started. I've read a number of papers, books and websites while trying to figure out the damage social media has done and is doing. Needless to say, these sorts of things tend to evolve into a lot of different directions. So much so that I decided that I want to try and reduce my reliance on my smartphone, apps and online accounts.  

I started by getting a used Pixel 2 (which, although old, was quite cheap and still suitable for my needs) and installing [Graphene OS](https://grapheneos.org/). This made me realize that I would need to give up or find alternatives to a lot of the apps I was using mostly because I didn't want to end up using microG to just install Facebook/Messenger/Whatsapp on my new device. I decided to close my Facebook account, create new email accounts, get rid of anything that I wasn't using as much and migrate to Signal, Vanadium, Tutanota and AP15. I had something to message people, I had an email account, I had a browser and I had a simple launcher. I ended up removing the background picture and settle for a black backround with red text. The end result? My 4 year old Pixel 2 lasts between 2 and 3 days on a single charge and I spend WAY less time on it.  

### But I need X/Y/Z for work!  

And I get that. I'm not saying this is what you should do, I'm just saying what **I** did and what worked for **me**. You should consider your own circumstances and see what works.  

After this stage was complete, I went ahead and started closing old accounts that I had signed up for, with some of them being used rarely and some of them having been accessed in years. Needless to say, this took a lot longer than setting up my phone (it's really... still ongoing). But it did feel good. It did feel good to rid myself of around 50-60 different accounts I hadn't used in years. The effects are double because apart from not getting spam or reminders from the most random of places, the chances that my details leak if those services are compromised are also greatly reduced. Whatever I couldn't delete or cancel, I just swapped over to random disposable email accounts and auto-generated passwords.  

Ok, I've gotten this far, but what about the "useful" stuff? What about cloud storage, contact syncing, note taking, SSO across different services? What about that? Well, I took a good hard look at what I was getting from these services. When was the last time I said "Thank GOD I had these pictures backed up on Google Photos"? Well, never, if I'm honest with myself. While I do have pictures and I do keep them backed up, instant sync was never an issue (again, this might be different for you!). So cloud storage is useful, but I wanted to keep a more centralized backup, a backup of my device, my photos and files in a secure way. Enter rclone.  

[Rclone](rclone.org/) was recommended by a friend and it has been a great tool. I can easily create and upload an encrypted backup to a cloud storage provider and then sync only changed files. Rclone is also essential in the backup process I use to backup my VPS.  

What about notes? Simplenote. What about the notes I would take when completing an online course? After using nuclino for some time, I found that it became quite sluggish over time. Having it open along with VS Code (Codium for those who don't want the Microsoft build) and Firefox, I would notice significant lag. There were other things I didn't like, but I won't go into that. While looking for alternatives I found [BookStackApp](https://www.bookstackapp.com/). Because it's self-hosted I also got the chance to explore that side of things. I always wanted to setup and manage a small Linux server, but never had a good enough reason to do so. This was the perfect scenario - from setting up BSA, to figuring out how to backup and where to backup my files, to just general remote server management.  

### Why change the website though?  

My previous website was written while I was still in university and I was trying to learn Spring Boot. It was a great experience from writing the code myself, to handling security, authentication, deployment and maintenance. But this kind of felt like a lot of work and the way it handled posts made me reticent to post more often. Five or so posts a year isn't much - not that I'd want these ramblings on a daily or even weekly basis, but there were a few ocassions when I didn't write anything just because of how I implemented the article writing side of things.  

On top of that there was the issue of having to have additional accounts, additional username/password combos for logging into the page, for database connections, for Heroku accounts and the list goes on. Due to these reasons (and a few others) I switched over to this.  

This page runs on [Hugo](https://gohugo.io/) and is deployed on Gitlab. That's it. I already had a Gitlab account, so that's not an additional account I need to know, there is no database and there is very little overhead. While I tried Jekyll previously, I found static site generators quite restrictive and my knowledge level was quite low (not that it's that much higher now), so I felt that I can't quite *control* it enough. With Hugo I told myself that I would start from scratch, make a theme myself and just have it running there for my restricted use case. After a week of work on the page while following the Hugo documentation and following some of [Mike Dane's](https://www.youtube.com/playlist?list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3) videos, here it is - black and white and red(ish).  

While I started writing this with some other ideas, I hope that this page will get a few more articles this year - if I write 6 I would have already beaten my previous record! I also want to fiddle with it more than I did with my previous Spring Boot page and use it to learn and improve on my HTML/CSS/JS knowledge. In any case, I guess the point I was trying to make is - less is more. I have more time to focus on what I care about and what I'm interest in, which is far more valuable than what I was getting from the services I gave up on.  

### Catalin