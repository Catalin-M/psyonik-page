---
title: "Mediocre – Don’t Look Up and The Matrix Resurrections"
date: 2021-12-28T07:54:24Z
draft: false
slug: "Mediocre – Don’t Look Up and The Matrix Resurrections"
summary: "Before I get started, if you haven't seen the movies and do not want spoilers, stop reading now. To sum up my thoughts on the two movies: they are bad, perfect examples of everything wrong with the current artistic environment. They are representative of an industry that has been going downhill for a few decades now, and not because streaming is taking over or because of piracy, but because it's producing *content* instead of art, because it puts monetary value above creative expression.  "  
---  
### Setup

Before I get started, if you haven't seen the movies and do not want spoilers, stop reading now. To sum up my thoughts on the two movies: they are bad, perfect examples of everything wrong with the current artistic environment. They are representative of an industry that has been going downhill for a few decades now, and not because streaming is taking over or because of piracy, but because it's producing *content* instead of art, because it puts monetary value above creative expression.  

![Blank image with text reading '404 - Originality not found'](/images/404notfound.png)  
##### Created by me in MSPaint  

The past decade has been a deluge of remakes, rehashed ideas made by people trained in economics, marketing and data analysis. A collection of trash that is meant to monetise, but offer no real artistic value to the audience. 

[Eight out of the top 10 grossing films between 2010-20 were sequels](https://en.wikipedia.org/wiki/2010s_in_film). The other two, *Black Panther* and *The Avengers* are part of the same universe.

We created a world where we use money to set the value of everything, including art. Why would I create something that isn't guaranteed to bring return on investment? How can one choose visual arts, literature or music, when all businesses hire data scientists, developers and engineers? But all those "I sure hope they open an art factory for that history of art degree of yours" jokes are coming back to bite us all, because no one seems to be concerned with the humanistic side of existence any more. 

Netflix and Amazon have also a huge blame for this, both churning out vast amounts of content. Whatever is least objectionable to ensure that people keep their eyes peeled on their screens as they hit "Skip intro" and "Next" mindlessly while ordering food, entertainment and things. We are becoming like our movies – flashy, popular, but completely devoid of substance, with no original thoughts, no passion, quite… lifeless and blah. Enter the Matrix... Resurrected.  

### Confrontation  

#### The Matrix Resurrections  

The title is a horrible misnomer. *The Matrix – Grave Robbers* would have been more in line with what the movie actually is. I'm not sure what Lana's plan was, if there was any at all, or if all of this is a play by Warner Bros to make some quick cash by pulling on the [“memberberries”](https://southpark.fandom.com/wiki/Memberberries) of those 30 and 40 year olds who were genuinely influenced by the original Matrix while growing up. I know they got a few bucks from me, so I guess the gambit paid off. 

I found *Matrix Resurrections* incoherent and almost impossible to follow. It uses the same social media news-feed approach of jumping from topic to topic without any breaks for thought, desperately trying to make sure that you stay entertained and that everyone is included. But the only reason you are still there is because you’re kept busy by an endless onslaught of "things happening". It doesn't matter what the "thing" is as long as it “is happening". 

*Matrix Resurrections* wants to bring a lot of new ideas to the franchise but fails miserably. Is this about consumerism? Is it about the fact that we're lost in our smart devices and some people feel completely left out and isolated when looking at their fellow humans staring into screens all day? Is it about female empowerment? Is it about the fact we get strawberries back? Is it fan service (hello, Trinity's husband, Chad - you know, that Chad, the John Wick Chad)? Is it crude language trying to appear hip (MILF???)? It's all those things and perhaps more.  

When we're not busy trying to follow one of the 3 or 4 subplots, we're being punished with some of the worst fight choreography that I have seen in a while. The camera is placed too close, and the fights are mediocre reenactments of previous movies. Smith seems to have gotten the Ken-doll treatment and offers little to no screen presence. In the previous films, when Hugo Weaving entered the stage there was a sense of gravitas attached to his scenes.  

Fake-Morpheus falls in the same boat. The original meeting between Neo and Morpheus in the old Matrix felt strange and eerie, leaving the viewer intrigued and focused. This was the person Neo searched for, for so long. The new Morpheus attempts to be funny, a weak proxy of the original, offering one of the least believable characters in the movie. At times, I wondered what his purpose was at all. Keanu Reeves and Carrie-Ann Moss have good chemistry, but it's quickly drowned out by the other things going on. The movie takes forever to get to them anyway, jumping through more narratives than I'd care to see in a movie twice as long.   

Think back to the original Matrix. Neo searches for meaning, Morpheus finds Neo, Neo finds meaning, Trinity and Neo find each other, Neo defeats nemesis and saves Morpheus/Trinity. The end. It is pretty easy to follow, because despite all the things that were revolutionary about the movie, you could have told a similar story in almost any environment and it would have worked. The ideas are so central to our existence and our humanity that they resonate past **bullet time**, CGI and kung-fu. 

Does the *Matrix Resurrections* have the same story flow? Neo is sad, someone is looking for him, Morpheus is sad or he isn't, but it seems he is. Trinity is married to John Wick guy. Trinity has kids. Also Neo goes to therapy. Therapy isn't therapy (or isn’t really working). Someone is mentioning the Matrix game and bullet time. At this point I struggle to remember the convoluted mess that followed, but what’s clear is that none of it works. 

While there are ideas worth exploring, they should be their own movies or be removed entirely, perhaps leaving the focus on huge focus change where Trinity emerges as an individual and not as an attachment to The One. But as things are, we're left with nothing, except anger and disappointment at this having been made in the first place.

#### Don't Look Up  

Before I dig into this one, I want to make it clear that I'm looking at Don't Look Up as a movie and not its metaphor to climate crisis and how we treat scientists. That deserves its very own discussion. Here I will focus on the movie as an artistic creation and draw some parallels to the current state of affairs, but I will not dive into the whole narrative.  

Structurally, *Don't Look Up* shares some of the same traits of Matrix Resurrections. Some ideas and topics pop up briefly and the movie seems to be desperate to throw things at us. While I did mention that this was an issue with Lana Watchowski's new Matrix, I didn't mention *why*.  

Slowing down a movie, taking a breather from the action allows the audience to go over what has happened so far. It also makes the subsequent scene have a stronger impact by contrast. To compare *Don't Look Up* with one of my favourite satires, *Dr Strangelove*, the latter has plenty of moments when it slows down. This is when the audience has time to realise the absolute insanity of the premise. We're given time to observe the characters, what has happened so far and try and guess where it's headed. We don't get the same treatment from the two newer titles.  

The overall tone of *Don't Look Up* feels very condescending. The movie looks at us as if we're guilty of the status quo. While to some extent that is true, it's a lot less likely that people will change or think about their actions if they're being treated like children. A collection of A-list actors is co-opted to turn the 2 hours and 40 minutes into a sermon. Each scene reeks of self-importance and superiority and we're constantly reminded how bad everyone is and how horrible things are and that only two people know the truth.  

Satire is about highlighting stupidity and human vices. The few moments when this shines through without it being part of a lecture is in the interactions between the president and her son. The moments seem genuinely ridiculous and I couldn't help but think of how absurd 2016-2020 felt. But those moments don't last and we're being yelled at and scolded minutes later.  

The problem with the comet metaphor is that it removes any relationship between cause and effect. We didn't end up here because of uncontrolled capitalism (as reflected by the tech mogul), we didn't end up here because of how we vote or our celebrity culture, it sort of just happened, through no fault of our own. *Dr Strangelove* or *Idiocracy* are clear that the current situation was caused by us or those we put in power. We caused the problem and whatever metaphor you might try to make loses its weight when you remove this causal relationship.  

Technology is only briefly mentioned, but there is no exploration of just how harmful it is, except for a short scene in which a new "smart device" that knows your emotions and keeps you happy is presented. Unfortunately, this is just the surface of an unbelievably complex issue that isn't addressed outside of that one short scene.

The movie isn't looking to highlight this, but aims at creating an **us** vs. **them** narrative. This antithesis is reflected in the relationship between the scientists and people on their side and those who oppose them. But satire doesn’t focus on breaking us apart, but highlight the common issues that we're all facing or ridicule serious shortcomings. A scientist who constantly force feeds us the same lines doesn't qualify as satire. It's just scolding. If it was, our scientist friends would have spent the whole movie speaking out against those who actually caused the current crisis – greedy, unhinged and uncontrolled capitalists. A system devoid of humanity and focused solely on profit. But that barely happens at all (maybe due to fear of upsetting some of those data scientists or venture capitalists connected to Netflix).

This narrative of blaming regular people for catastrophes has been exploding recently. But it ignores the complexities of human nature and existence. It ignores that depending on where you sit on [Maslow's pyramid](https://en.wikipedia.org/wiki/Maslow%27s_hierarchy_of_needs) you might not have the time, energy or resources to constantly fight the technology meant to tie you to the screen 24/7. You might not have the time to exercise or the energy to constantly keep your guard up. Those trying to stay outside filter bubbles know – it’s a full-time job.   

The movie ignores the slow death of democracy or the kidnapping of journalism and its conversion into sensationalism and clickbait. It ignores that we didn’t choose celebrity culture, but we were pushed into it. We were fed the murder, the shock, the horror, the gossip constantly while we had our value system realigned over decades. Yet this is entirely ignored over and over again and we're treated like accomplices.   

Satire is meant to make us think, to make us consider what is wrong around us and try to better ourselves and the society and world around us. But how many times has yelling at someone had a positive effect? How many times did you change your behaviour when someone was treating you like a child as opposed to an equal participant in a discussion?

If you want a better satire about our future, watch *Idiocracy*. It's simpler, it's funnier and it doesn't attempt to preach, it understands that we're all the result of our circumstances and sometimes these circumstances collude into making us dumber by the day. It also represents a more realistic view of our future where we've evolved into professional consumers, sitting on our toilet+La-z-Boy combo.

### Resolution  

What are we left with? If we're not struggling to make sense of another get-rich-quick remake, then we're being told we're stupid or that we don't act. *Don't Look Up* deepens the gap between each other. *Matrix Resurrections* doesn't have any regard for the viewer from a storytelling perspective and seems to be here just to cash in on the memory of a great story from over 20 years ago.

Both movies seem the result of someone feeding keywords into an algorithm that spits out a script based on those keywords. We no longer have to worry about mindlessly scrolling through an infinite stream – they do it for us.

What if *Matrix Resurrections* would have focused solely on Trinity's empowerment? What if the subplots are focused on Neo escaping, without it trying to revive all the characters and jamming them into each scene? What if the new characters actually matter and really contribute to the story? What if that empowerment isn’t turned into a joke in the last few minutes of the movie with Trinity and Neo hacking away at an AI – contrast this with the ending in the first Matrix where an almost fully formed Neo sends out a stern warning? What if Trinity stands on her own two feet and can finally be an equal to Neo and doesn’t depend on him like some sort of appendix?

What if *Don't Look Up* treats us as equal members in a conversation, trying to figure out how to progress and solve these massive challenges ahead? What if the movie takes into account the fact that people are concerned with the many crises we're currently facing, but they are up against undemocratic systems bribed by large commercial interests? What if you could actually recommend this movie to all your friends and acquaintances to share common ground as opposed to widening the rift between you and your right-leaning friend (P.S. I'm kidding, it's obvious that none of us reaches across the left/right divide anymore)? What if it made us all laugh?

But both of these paths require art to be made by and for people and not profit at all cost. They require a true concern and awareness of the viewer. Art needs to me made by artists and not by algorithms and businesses. But I guess we "got what we deserve"?

### Catalin