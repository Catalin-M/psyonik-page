---
title: "Time and the new meta of working from home"
date: 2020-07-21T10:14:00Z
draft: false
summary: "Wow, did the past few months just fly by. It seems only a couple of days ago that I was wrapping up some work for school to get that neural networks coursework out of the way and bam, it's four months later and I'm studying them again, this time though for exams.

Things changed, massively so, in these months and each time I wanted to slow down and take a deep breath to write down my thoughts, I never got the chance. But I do now!"
---

### Or how there isn't any (time)  

Wow, did the past few months just fly by. It seems only a couple of days ago that I was wrapping up some work for school to get that neural networks coursework out of the way and bam, it's four months later and I'm studying them again, this time though for exams.

Things changed, massively so, in these months and each time I wanted to slow down and take a deep breath to write down my thoughts, I never got the chance. But I do now!  

![Image of someone working on a PC](/images/working_on_pc.jpg)  
##### Credit for image goes to [Joshua Rawson](https://unsplash.com/@joshrh19)  

So what happened these last few months?

A lot. A lot has happened. The changes that this little virus brought about have changed the way we work and study. There is now an entirely new paradigm being pushed of working from home, of merging our work with our personal spaces. Which isn't necessarily bad. Necessarily...

My issues here have stemmed from the fact that over the past few months I felt how work crept into the little free time I had. I do save time commuting now, but that time tends to be spent on other things and more often than not, it gets spent on work. Work and play have now become inextricably intertwined and it sometimes feels very difficult to disconnect or separate the two.

While there is truth that we are now saving quite a bit of time and money by not going to work, paying for bus/train fares or gas, employers are saving way more with utility costs down to almost nothing during these times. Some office spaces get repurposed and companies are both benefiting from the savings of not having to heat/cool/light their offices while using these for other activities that can generate additional income. You, as the employee, do not see any of those financial benefits.

Some employers do end up paying for a portion of the employees utility costs and some even pay for new desks/chairs/monitors to be sent to the employees home. But not all. Too often have I heard of stories of employees hunched over their kitchen table, on chairs that were designed to hold you for the 20-30 minutes you eat, working for 8-9 hours.

Personally, I was lucky to a certain extent and this entire cavalcade of the horrible and horrific turned out well. I had extra time to prepare for exams and my final project, I had more time and saved money, but I can't shake the feeling that my living room is now my office. That when I step into it each morning, this is a workplace for a good chunk of the day. While I always (and I do mean ALWAYS) loved working from home, I enjoyed the fact that it was a choice or that the role itself was meant as a WFH role to begin with.

Having spent around two years writing articles for a variety of blogs and tech websites, I had gotten used to working from home. I had gotten used to waking up early and just crawling over to my desk to see what has happened while I was asleep. But then, I could just get up and leave and get a coffee at any point in time. I could pickup work at 12 at night or leave on Thursday for a long weekend with some friends. That life and that work-life balance was a choice. This isn't. And that's where the problem lies.

I believe we will be facing a huge wave of issues down the road. We will have difficulties finding that balance again and more employers will realise that they can offload their costs to their employees while work will creep into even the privacy of our homes (think about how you need to change your camera angles or setup a wall behind yourself to hide the rest of your home). We will have difficulties returning to normality or going back to our offices and employers might not want us to do so anyway - one company where I live saves on average £100,000 per week in heating/electricity costs. Do you think they will want to give that up if they see that productivity has not slumped?

The solution or one of the ones I have considered, is that employers should foot the costs of upgrading homes. Those renting 1 bedroom apartments should be given money to move to 2 bedroom apartments to allow the conversion of a room into an office. To create that physical separation between work and life, because as it stands, the two have merged into one.

But all of these things are unbelievably complicated and I am in no way qualified to touch on them in any greater detail. What I do know for a fact is that my day, although longer now, seems shorter. And I don't wish to keep it this way and neither should you.   

### What's next for me  

Right now I have a couple of exams left, plenty of work and very little time, so personally, I look forward to a bit of time off. I want to regain a bit of control and focus on upgrading this page :) I want to migrate it to a .Net Core backend and maybe create a Javascript front-end. I want to regain a bit of time and focus on studying the things I want now that I am close to finishing the things others told me to study :) The irony of education - it opens up your thirst to study really interesting things, but limits you to study only what it deems important. I guess it's part of the process and although I am grateful to my school, I am very much looking forward to opening up a book on algorithms and another one on JS :)  

### Catalin