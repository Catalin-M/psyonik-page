---
title: "Solving big problems one at a time"
date: 2020-01-17T11:53:00Z
draft: false
summary: "I was greeted a few days ago by a quick message underneath my search box asking me if I was up for a challenge. I didnt' take a screenshot at the time, but the idea stands and the message was similar to the one below (the image is taken from [this](https://medium.com/magentacodes/things-you-should-know-about-google-foobar-invitation-703a535bf30f) Medium article about Google Foobar and all credit goes to the original source). "
---

### Going through Google Foobar exercises  

I was greeted a few days ago by a quick message underneath my search box asking me if I was up for a challenge. I didnt' take a screenshot at the time, but the idea stands and the message was similar to the one below (the image is taken from [this](https://medium.com/magentacodes/things-you-should-know-about-google-foobar-invitation-703a535bf30f) Medium article about Google Foobar and all credit goes to the original source).  

![Screenshot showing the message received from Google Foobar on the Google search page](/images/google_foobar.jpeg)  

I wasn't sure what I was looking at but after googling around a bit I found that this was a challenge that you can receive at random from Google.  

The messages that showed on the page were quite funny, which helped me get into the zone like an old school text-based MUD. There is also the question of privacy that emerged a bit in the back of my head and the implications of actually having access to the most commonly used search engine to select potential engineers.  

I don't think my skill level is anywhere near that of getting hired, but it makes me think - what if you are looking for job boards for your area, your recent history shows the kind of development concepts you were looking for and now you could, in theory, look at a potential future employee with a more rigorous background check than any other company could achieve.  

This is, of course, if you were to hold that in-detail information :)  

### The challenges  

I have currently only submitted the level 1 and level 2 challenges (there are 5 levels in total), but they were quite fun in the way the concepts are presented as they were part of this game. I won't go into the details of the questions and there's plenty more to complete, but what I really enjoyed was the fact that I could work on these without any sort of pressure outside of time constraints (I got 24 hours for the first challenge, 72 hours for the subsequent level 2 challenges each).  

Due to the nature of the challenge itself, I wanted to make sure I complete them with no outside help. My searches were reserved to the Java documentation. It's not necessarily that I think that I am getting even an interview with Google,
but rather this felt like a personal challenge. It felt like when I startup a game of Dota 2 and I get put in the hard support role, my favorite heroes are picked and I need to dive in with both feet using a new hero but still wanting to win.  

I am currently just overwhelmed by the sheer amount of work I have to do both at work and at home to compelte courseworks for school, so it was challenging to find even a few minutes to focus on the problems. Despite that, they were really entertaining, considering that my algorithm skills are quite lacking and considering that I only did a few HackerRank questions.  

As a note for those who get the Foobar challenge - use paper/whiteboards and whatever else helps you. Don't StackOverflow, don't search the 7 seas of the internet, just take this for what it can be - a fun puzzle. It's been some time since I did a challenge like this for fun and I must say, the only thing I'm waiting for is to finish my coursework and exams and dive back into finishing the next levels or at least give my best shot.  

### Moving forward  

I think the biggest thing for me is that this gave me motivation to start doing these sort of challenges outside of work, as a way to improve my thinking and most importantly, to remind me why I enjoy coding in the first place - not knowing something and going step by step until you kinda know something.  

It made me feel like the first time I loaded up Starcraft and was blown away by the damage of Marines and Firebats. It 
reminded me of going through a Windows 98 installation for the very first time when I was in 7th grade, it reminded me of what computers really mean to me - a way to escape reality, a way to fall into this big, deep pond of knowledge and mistery and a way to feel trully happy.  

So for that... thank you Google. I'm not sure what the darker implications could be... I honestly don't. A part of me is happy, but another part thinks that this is a strange moment in our existence as we might be reaching points where machines and algorithms start knowing us and understanding us better than we do... Or maybe I'm looking too much into it :) In any case, if you get the invite to the challenge, do it; do it alone; do it yourself; do it because it's fun to learn new things despite what you see on social media, online in general and in the news. 

### Catalin